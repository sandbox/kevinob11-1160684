Drupal.behaviors.inline_form_focus = function () {
	$("#page").ajaxComplete(
		function (e, xhr, settings) {
			if (settings.url.match("inline_ajax_forms")) {
				$(".inline-ajax-form-container .form-item:first input, .inline-ajax-form-container .form-item:first textarea").focus();
				$(".inline-ajax-form-container input").bind('keypress', function (event) {
					if (event.which == '13') {
						$(".inline-ajax-form-container input.form-submit").click();
					}
				});
			}
		}
	);
}