Drupal.behaviors.dialog_form_focus = function () {
	$("#page").ajaxComplete(
		function (e, xhr, settings) {
			if (settings.url.match("inline_ajax_forms_dialog")) {
				$("#dialog-main .form-item:first input, #dialog-main .form-item:first textarea").focus();
				$("#dialog-main input").bind('keypress', function (event) {
					if (event.which == '13') {
						$(".ui-dialog-buttonpane button").click();
					}
				});
			}
		}
	);
}